package com.delayMessage;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

public class Producer {
    public static void main(String[] args) throws Exception {

        //创建发送消息对象
        DefaultMQProducer producer = new DefaultMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        for (int i = 1; i <= 10; i++) {
            //构建消息，指定topic和body
            Message msg = new Message("topic1", ("延时消息：" + i).getBytes());

            //设置延迟消息等级
            msg.setDelayTimeLevel(3);

            //发送消息
            SendResult sendResult = producer.send(msg);
            System.out.println("sendResult = " + sendResult);
        }

        //关闭连接
        producer.shutdown();

    }
}
