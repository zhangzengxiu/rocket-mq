package com.transtion;

import org.apache.rocketmq.client.producer.*;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;

public class Producer {

    /**
     * 事务补偿过程
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        //事务消息使用的生产者是TransactionMQProducer
        TransactionMQProducer producer = new TransactionMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        //添加本地事务对应的监听
        producer.setTransactionListener(new TransactionListener() {

            long start;

            //正常事务过程
            public LocalTransactionState executeLocalTransaction(Message message, Object o) {
                start = System.currentTimeMillis();
                //提交事务
                return LocalTransactionState.UNKNOW;
            }

            //事务补偿过程，如果事务补偿过程状态返回仍然是不断进行事务补偿，测试结论每60秒发起一次
            public LocalTransactionState checkLocalTransaction(MessageExt messageExt) {
                long time = (System.currentTimeMillis() - start) / 1000;
                System.out.println("time = " + time);
                System.out.println("事务补偿过程执行了");
                return LocalTransactionState.COMMIT_MESSAGE;//提交
            }
        });

        //构建消息，指定topic和body
        Message msg = new Message("topic10", "hello rocketmq".getBytes());
        //发送事务消息
        TransactionSendResult sendResult = producer.sendMessageInTransaction(msg, null);
        System.out.println("sendResult = " + sendResult);

        //关闭连接
        // producer.shutdown();

    }

    /**
     * 正常事务提交事务，无事务补偿过程
     *
     * @param args
     * @throws Exception
     */
    public static void main1(String[] args) throws Exception {

        //事务消息使用的生产者是TransactionMQProducer
        TransactionMQProducer producer = new TransactionMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        //添加本地事务对应的监听
        producer.setTransactionListener(new TransactionListener() {
            //正常事务过程
            public LocalTransactionState executeLocalTransaction(Message message, Object o) {
                //提交事务
                return LocalTransactionState.COMMIT_MESSAGE;
            }

            //事务补偿过程
            public LocalTransactionState checkLocalTransaction(MessageExt messageExt) {
                return null;
            }
        });

        //构建消息，指定topic和body
        Message msg = new Message("topic10", "hello rocketmq".getBytes());
        //发送消息
        TransactionSendResult sendResult = producer.sendMessageInTransaction(msg, null);
        System.out.println("sendResult = " + sendResult);

        //关闭连接
        producer.shutdown();

    }

    /**
     * 正常事务事务回滚过程
     *
     * @param args
     * @throws Exception
     */
    public static void main2(String[] args) throws Exception {

        //事务消息使用的生产者是TransactionMQProducer
        TransactionMQProducer producer = new TransactionMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        //添加本地事务对应的监听
        producer.setTransactionListener(new TransactionListener() {
            //正常事务过程
            public LocalTransactionState executeLocalTransaction(Message message, Object o) {
                //回滚事务
                return LocalTransactionState.ROLLBACK_MESSAGE;
            }

            //事务补偿过程
            public LocalTransactionState checkLocalTransaction(MessageExt messageExt) {
                return null;
            }
        });

        //构建消息，指定topic和body
        Message msg = new Message("topic10", "hello rocketmq".getBytes());
        //发送消息
        TransactionSendResult sendResult = producer.sendMessageInTransaction(msg, null);
        System.out.println("sendResult = " + sendResult);

        //关闭连接
        producer.shutdown();

    }
}
