package com.order;

import com.order.domain.Order;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试消息的顺序性
 */
public class Producer {
    public static void main(String[] args) throws Exception {

        //创建发送消息对象
        DefaultMQProducer producer = new DefaultMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        ArrayList<Order> orderList = new ArrayList<Order>();

        Order order1 = new Order();
        order1.setId("a");
        order1.setMsg("主订单-1");
        orderList.add(order1);

        Order order2 = new Order();
        order2.setId("a");
        order2.setMsg("子订单-2");
        orderList.add(order2);

        Order order3 = new Order();
        order3.setId("a");
        order3.setMsg("支付订单-3");
        orderList.add(order3);

        Order order4 = new Order();
        order4.setId("a");
        order4.setMsg("推送消息-4");
        orderList.add(order4);

        Order order5 = new Order();
        order5.setId("b");
        order5.setMsg("主订单-1");
        orderList.add(order5);

        Order order6 = new Order();
        order6.setId("b");
        order6.setMsg("子订单-2");
        orderList.add(order6);

        Order order7 = new Order();
        order7.setId("c");
        order7.setMsg("主订单-1");
        orderList.add(order7);

        Order order8 = new Order();
        order8.setId("c");
        order8.setMsg("子订单-2");
        orderList.add(order8);

        Order order9 = new Order();
        order9.setId("c");
        order9.setMsg("支付订单-3");
        orderList.add(order9);

        //设置消息进入到指定的消息队列中
        for (final Order order : orderList) {
            //构建消息
            Message msg = new Message("orderTopic", order.toString().getBytes());

            //发送时要指定对应的消息队列选择器
            SendResult sendResult = producer.send(msg, new MessageQueueSelector() {
                public MessageQueue select(List<MessageQueue> list, Message message, Object o) {

                    //根据发送的消息的不同，选择不同的消息队列
                    //根据id来选择一个消息队列的对象，并返回->id得到int值，作为索引
                    int index = order.getId().hashCode() % list.size();

                    //返回一个消息队列MessageQueue
                    return list.get(index);

                }
            }, null);

            System.out.println("sendResult = " + sendResult);
        }

        //关闭连接
        producer.shutdown();

    }
}