package com.filterBySql;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

public class Producer {
    public static void main(String[] args) throws Exception {

        //创建发送消息对象
        DefaultMQProducer producer = new DefaultMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        //构建消息，指定topic和body
        Message msg = new Message("topic1", "tag1", "消息过滤tag1消息".getBytes());

        //为消息添加属性
        msg.putUserProperty("vip","1");
        msg.putUserProperty("age","20");

        //发送消息
        SendResult sendResult = producer.send(msg);

        System.out.println("sendResult = " + sendResult);

        //关闭连接
        producer.shutdown();

    }
}
