package com.mul;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.util.ArrayList;

public class Producer {
    public static void main(String[] args) throws Exception {

        //创建发送消息对象
        DefaultMQProducer producer = new DefaultMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        ArrayList<Message> messageList = new ArrayList<Message>();

        //构建消息，指定topic和body
        Message msg1 = new Message("topic1", ("批量消息：" + 1).getBytes());
        Message msg2 = new Message("topic1", ("批量消息：" + 2).getBytes());
        Message msg3 = new Message("topic1", ("批量消息：" + 3).getBytes());
        Message msg4 = new Message("topic1", ("批量消息：" + 4).getBytes());

        messageList.add(msg1);
        messageList.add(msg2);
        messageList.add(msg3);
        messageList.add(msg4);

        //发送消息
        SendResult sendResult = producer.send(messageList);

        System.out.println("sendResult = " + sendResult);

        //关闭连接
        producer.shutdown();

    }
}
